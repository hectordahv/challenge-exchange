#FROM ubuntu:latest
#RUN apt-get -y update
#RUN apt-get -y upgrade

#RUN apt-get install -y openjdk-11-jdk

FROM openjdk:latest
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar", "--spring.datasource.url=jdbc:mysql://host.docker.internal:3306/db"]