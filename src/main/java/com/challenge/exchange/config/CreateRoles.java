package com.challenge.exchange.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.challenge.exchange.security.entities.Role;
import com.challenge.exchange.security.enums.RoleName;
import com.challenge.exchange.security.services.RoleService;

@Component
public class CreateRoles implements CommandLineRunner{

  @Autowired
  RoleService roleService;

  @Override
  public void run(String... args) throws Exception {
    /*
    Role roleAdmin = new Role(RoleName.ROLE_ADMIN);
    Role roleUser = new Role(RoleName.ROLE_USER);

    roleService.addRole(roleAdmin);
    roleService.addRole(roleUser);
     */
  }  
}
