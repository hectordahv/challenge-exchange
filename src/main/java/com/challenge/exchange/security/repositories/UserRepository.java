package com.challenge.exchange.security.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.exchange.security.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
  public abstract Optional<User> findByEmail(String email);
  public abstract Optional<User> findByUserName(String userName);
  public boolean existsByEmail(String email);
  public boolean existsByUserName(String userName);
}
