package com.challenge.exchange.security.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.exchange.security.entities.Role;
import com.challenge.exchange.security.enums.RoleName;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  public abstract Optional<Role> findByRoleName(RoleName roleName);
}
