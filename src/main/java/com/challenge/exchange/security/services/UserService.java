package com.challenge.exchange.security.services;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.exchange.security.entities.User;
import com.challenge.exchange.security.repositories.UserRepository;

@Service
@Transactional
public class UserService {
  @Autowired
  UserRepository userRepository;

  public ArrayList<User> listUsers() {
    return (ArrayList<User>) userRepository.findAll();
  }

  public void addUser(User user) {
    userRepository.save(user);
  } 

  public Optional<User> findByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  public Optional<User> findByUserName(String userName) {
    return userRepository.findByUserName(userName);
  }

  public boolean existsByEmail(String email) {
    return userRepository.existsByEmail(email);
  }

  public boolean existsByUserName(String userName) {
    return userRepository.existsByUserName(userName);
  }
}
