package com.challenge.exchange.security.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.exchange.security.entities.Role;
import com.challenge.exchange.security.enums.RoleName;
import com.challenge.exchange.security.repositories.RoleRepository;

@Service
@Transactional
public class RoleService {
  @Autowired
  RoleRepository roleRepository;

  public Optional<Role> findByRoleName(RoleName name) {
    return roleRepository.findByRoleName(name);
  }

  public void addRole(Role role) {
    roleRepository.save(role);
  }
}
