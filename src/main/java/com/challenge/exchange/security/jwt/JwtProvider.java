package com.challenge.exchange.security.jwt;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.challenge.exchange.security.entities.PrincipalUser;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtProvider {
  
  private final static Logger logger = LoggerFactory.getLogger(JwtProvider.class);

  @Value("${jwt.secret}")
  private String secret;

  @Value("${jwt.expiration}")
  private int jwtExpirationInMs;

  public String generateToken(Authentication auth) {
    PrincipalUser principalUser = (PrincipalUser) auth.getPrincipal();
    return Jwts.builder().setSubject(principalUser.getUsername())
      .setIssuedAt(new Date())
      .setExpiration(new Date(new Date().getTime() + jwtExpirationInMs * 1000))
      .signWith(SignatureAlgorithm.HS512, secret).compact();
  }

  public String getUserNameFromToken (String authToken) {
    return Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken).getBody().getSubject();
  }

  public boolean validateToken(String authToken) {
    try {
      Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken);
      return true;
    } catch (MalformedJwtException ex) {
      logger.error("JWT token is malformed.");
    } catch (UnsupportedJwtException ex) {
      logger.error("Unsupported JWT token");
    } catch (IllegalArgumentException ex) {
      logger.error("JWT claims string is empty.");
    } catch (ExpiredJwtException ex) {
      logger.error("JWT token is expired.");
    }
    return false;
  }
}
