package com.challenge.exchange.security.controllers;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.exchange.dto.MessageDto;
import com.challenge.exchange.security.dtos.AddUserDto;
import com.challenge.exchange.security.dtos.JwtDto;
import com.challenge.exchange.security.dtos.LoginDto;
import com.challenge.exchange.security.entities.Role;
import com.challenge.exchange.security.entities.User;
import com.challenge.exchange.security.enums.RoleName;
import com.challenge.exchange.security.jwt.JwtProvider;
import com.challenge.exchange.security.services.RoleService;
import com.challenge.exchange.security.services.UserService;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin
public class UserController {
  @Autowired
  UserService userService;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  RoleService roleService;

  @Autowired
  JwtProvider jwtProvider;

  @PostMapping("/register")
  public ResponseEntity<?> addUser(@Valid @RequestBody AddUserDto newUser, BindingResult result) {
    if (result.hasErrors()) {
      return new ResponseEntity<>(new MessageDto("Request Invalid"), HttpStatus.BAD_REQUEST);
    }
    if (userService.existsByUserName(newUser.getUserName())) {
      return new ResponseEntity<>(new MessageDto("User already exists"), HttpStatus.BAD_REQUEST);
    }
    if (userService.existsByEmail(newUser.getEmail())) {
      return new ResponseEntity<>(new MessageDto("Email already exists"), HttpStatus.BAD_REQUEST);
    }
    User user = new User(newUser.getUserName(), newUser.getEmail(), passwordEncoder.encode(newUser.getPassword()));
    
    Set<Role> roles = new HashSet<>();
    roles.add(roleService.findByRoleName(RoleName.ROLE_USER).get());

    if(newUser.getRoles().contains("admin")) {
      roles.add(roleService.findByRoleName(RoleName.ROLE_ADMIN).get());
    }

    user.setRoles(roles);
    userService.addUser(user);

    return new ResponseEntity<>(new MessageDto("User created"), HttpStatus.OK);
  }

  @PostMapping("/login")
  public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginDto loginUser, BindingResult result) {
    if (result.hasErrors()) {
      return new ResponseEntity(new MessageDto("Request Invalid"), HttpStatus.BAD_REQUEST);
    }

    Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUserName(), loginUser.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(auth);

    String jwt = jwtProvider.generateToken(auth);

    UserDetails userDetails = (UserDetails) auth.getPrincipal();

    JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), userDetails.getAuthorities());

    return new ResponseEntity<>(jwtDto, HttpStatus.OK);
  }

}
