package com.challenge.exchange.security.enums;

public enum RoleName {
  ROLE_ADMIN, ROLE_USER
}
