package com.challenge.exchange.security.dtos;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class JwtDto {
  private String token;
  private String type = "Bearer";
  private String userName;
  private Collection<? extends GrantedAuthority> authorities;

  public JwtDto() {
  }

  public JwtDto(String token, String userName, Collection<? extends GrantedAuthority> authorities) {
    this.token = token;
    this.userName = userName;
    this.authorities = authorities;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Collection<? extends GrantedAuthority> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
    this.authorities = authorities;
  }
}
