package com.challenge.exchange.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.exchange.entities.ExchangeRate;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long> {
  public abstract boolean existsByCurrencyFromAndCurrencyTo(String currencyFrom, String currencyTo);
  public abstract List<ExchangeRate> findByCurrencyFromAndCurrencyTo(String currencyFrom, String currencyTo);

  public abstract ExchangeRate findTopByOrderByIdDesc();
}
