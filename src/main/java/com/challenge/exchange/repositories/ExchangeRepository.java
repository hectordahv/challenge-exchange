package com.challenge.exchange.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.exchange.entities.Exchange;

@Repository
public interface ExchangeRepository extends JpaRepository<Exchange, Long> {
  public abstract List<Exchange> findByCurrencyFrom(String currencyFrom);
  public abstract List<Exchange> findByCurrencyTo(String currencyTo);
  public abstract List<Exchange> findByExchangeBy(String exchangeBy);
  public abstract List<Exchange> findByCurrencyFromAndCurrencyTo(String currencyFrom, String currencyTo);
}
