package com.challenge.exchange.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.exchange.entities.Currency;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {
  public abstract boolean existsByCode(String code);
}
