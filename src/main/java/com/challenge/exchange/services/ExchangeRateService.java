package com.challenge.exchange.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.exchange.entities.ExchangeRate;
import com.challenge.exchange.repositories.ExchangeRateRepository;

@Service
@Transactional
public class ExchangeRateService {

  @Autowired
  ExchangeRateRepository exchangeRateRepository;

  public void addExchange(ExchangeRate exchangeRate) {
    exchangeRateRepository.save(exchangeRate);
  }

  public Optional<ExchangeRate> findByCurrencyFromAndCurrencyTo(String currencyFrom, String currencyTo) {
    List<ExchangeRate> exchangesRate = exchangeRateRepository.findByCurrencyFromAndCurrencyTo(currencyFrom, currencyTo);
    return exchangesRate.stream().reduce((first, second) -> second);
  }

  public boolean existsByCurrencyFromAndCurrencyTo(String currencyFrom, String currencyTo) {
    return exchangeRateRepository.existsByCurrencyFromAndCurrencyTo(currencyFrom, currencyTo);
  }
  
  public Optional<ExchangeRate> findById(Long id) {
    return exchangeRateRepository.findById(id);
  }

  public ExchangeRate findLast() {
    return exchangeRateRepository.findTopByOrderByIdDesc();
  }
}
