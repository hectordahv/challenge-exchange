package com.challenge.exchange.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.exchange.entities.Exchange;
import com.challenge.exchange.repositories.ExchangeRepository;

@Service
@Transactional
public class ExchangeService {
  
  @Autowired
  ExchangeRepository exchangeRepository;

  public void addExchange(Exchange exchangeRate) {
    exchangeRepository.save(exchangeRate);
  }

  public List<Exchange> findAll() {
    return exchangeRepository.findAll();
  }

  public List<Exchange> findByCurrencyFrom(String currencyFrom) {
    return exchangeRepository.findByCurrencyFrom(currencyFrom);
  }

  public List<Exchange> findByCurrencyTo(String currencyTo) {
    return exchangeRepository.findByCurrencyTo(currencyTo);
  }

  public List<Exchange> findByExchangeBy(String exchangeBy) {
    return exchangeRepository.findByExchangeBy(exchangeBy);
  }

  public List<Exchange> findByCurrencyFromAndCurrencyTo(String currencyFrom, String currencyTo) {
    return exchangeRepository.findByCurrencyFromAndCurrencyTo(currencyFrom, currencyTo);
  }

}
