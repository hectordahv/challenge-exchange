package com.challenge.exchange.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.exchange.entities.Currency;
import com.challenge.exchange.repositories.CurrencyRepository;

@Service
@Transactional
public class CurrencyService {
  @Autowired
  CurrencyRepository currencyRepository;

  public void addCurrency(Currency currency) {
    currencyRepository.save(currency);
  }

  public boolean existsByCode(String code) {
    return currencyRepository.existsByCode(code);
  }

  public List<Currency> listCurrencies () {
    return currencyRepository.findAll();
  }

}
