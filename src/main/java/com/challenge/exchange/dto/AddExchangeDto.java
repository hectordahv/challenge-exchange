package com.challenge.exchange.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

public class AddExchangeDto {
  @NotBlank
  private String currencyFrom;

  @NotBlank
  private String currencyTo;

  @NotBlank
  @PositiveOrZero
  private Double amount;

  public AddExchangeDto() {
  }

  public AddExchangeDto(@NotBlank String currencyFrom, @NotBlank String currencyTo, @NotBlank Double amount) {
    this.currencyFrom = currencyFrom;
    this.currencyTo = currencyTo;
    this.amount = amount;
  }

  public String getCurrencyFrom() {
    return currencyFrom;
  }

  public void setCurrencyFrom(String currencyFrom) {
    this.currencyFrom = currencyFrom;
  }

  public String getCurrencyTo() {
    return currencyTo;
  }

  public void setCurrencyTo(String currencyTo) {
    this.currencyTo = currencyTo;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }
}
