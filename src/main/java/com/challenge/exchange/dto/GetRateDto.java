package com.challenge.exchange.dto;

import javax.validation.constraints.NotBlank;

public class GetRateDto {

  @NotBlank
  private String currencyFrom;

  @NotBlank
  private String currencyTo;

  public GetRateDto() {
  }

  public GetRateDto(@NotBlank String currencyFrom, @NotBlank String currencyTo) {
    this.currencyFrom = currencyFrom;
    this.currencyTo = currencyTo;
  }

  public String getCurrencyFrom() {
    return currencyFrom;
  }

  public void setCurrencyFrom(String currencyFrom) {
    this.currencyFrom = currencyFrom;
  }

  public String getCurrencyTo() {
    return currencyTo;
  }

  public void setCurrencyTo(String currencyTo) {
    this.currencyTo = currencyTo;
  }
  
}
