package com.challenge.exchange.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

public class AddExchangeRateDto {
  @NotBlank
  private String currencyFrom;

  @NotBlank
  private String currencyTo;

  @NotBlank
  @PositiveOrZero
  private Double rate;

  public AddExchangeRateDto() {
  }

  public AddExchangeRateDto(@NotBlank String currencyFrom, @NotBlank String currencyTo, @NotBlank Double rate) {
    this.currencyFrom = currencyFrom;
    this.currencyTo = currencyTo;
    this.rate = rate;
  }

  public String getCurrencyFrom() {
    return currencyFrom;
  }

  public void setCurrencyFrom(String currencyFrom) {
    this.currencyFrom = currencyFrom;
  }

  public String getCurrencyTo() {
    return currencyTo;
  }

  public void setCurrencyTo(String currencyTo) {
    this.currencyTo = currencyTo;
  }

  public Double getRate() {
    return rate;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }
}
