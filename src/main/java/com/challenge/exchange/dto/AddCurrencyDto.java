package com.challenge.exchange.dto;

import javax.validation.constraints.NotBlank;

public class AddCurrencyDto {
  @NotBlank
  private String code;

  @NotBlank
  private String name;

  @NotBlank
  private String symbol;

  public AddCurrencyDto() {
  }

  public AddCurrencyDto(@NotBlank String code, @NotBlank String name, @NotBlank String symbol) {
    this.code = code;
    this.name = name;
    this.symbol = symbol;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }
  
}
