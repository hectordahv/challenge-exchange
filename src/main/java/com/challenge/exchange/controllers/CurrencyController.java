package com.challenge.exchange.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.exchange.dto.AddCurrencyDto;
import com.challenge.exchange.dto.MessageDto;
import com.challenge.exchange.entities.Currency;
import com.challenge.exchange.services.CurrencyService;

@RestController
@RequestMapping("/api/currency")
@CrossOrigin
public class CurrencyController {
  @Autowired
  CurrencyService currencyService;

  @GetMapping("/list")
  public ResponseEntity<List<Currency>> getAllCurrencies() {
    return new ResponseEntity<>(currencyService.listCurrencies(), HttpStatus.OK);
  }

  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/create")
  public ResponseEntity<?> addCurrency(@Valid @RequestBody AddCurrencyDto newCurrency, BindingResult result) {
    if (result.hasErrors()) {
      return new ResponseEntity<>(new MessageDto("Request Invalid"), HttpStatus.BAD_REQUEST);
    }
    if (currencyService.existsByCode(newCurrency.getCode())) {
      return new ResponseEntity<>(new MessageDto("Currency already exists"), HttpStatus.BAD_REQUEST);
    }
    Currency currency = new Currency(newCurrency.getCode(), newCurrency.getName(), newCurrency.getSymbol());
    currencyService.addCurrency(currency);
    return new ResponseEntity<>(new MessageDto("Currency created"), HttpStatus.OK);
  }
}
