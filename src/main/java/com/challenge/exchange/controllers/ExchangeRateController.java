package com.challenge.exchange.controllers;

import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.exchange.dto.AddExchangeRateDto;
import com.challenge.exchange.dto.GetRateDto;
import com.challenge.exchange.dto.MessageDto;
import com.challenge.exchange.entities.ExchangeRate;
import com.challenge.exchange.services.ExchangeRateService;

@RestController
@RequestMapping("/api/exchange-rate")
@CrossOrigin
public class ExchangeRateController {
  @Autowired
  ExchangeRateService exchangeRateService;

  @PostMapping("/find")
  public ResponseEntity<?> findByCurrencyFromAndCurrencyTo(@Valid @RequestBody GetRateDto getRateDto,
      BindingResult result) {
        if(!exchangeRateService.existsByCurrencyFromAndCurrencyTo(getRateDto.getCurrencyFrom(), getRateDto.getCurrencyTo())) {
          return new ResponseEntity<>(new MessageDto("Exchange rate not found"), HttpStatus.BAD_REQUEST);
        }
      Optional<ExchangeRate> exchangeRate = exchangeRateService.findByCurrencyFromAndCurrencyTo(getRateDto.getCurrencyFrom(), getRateDto.getCurrencyTo());
      return new ResponseEntity<>(exchangeRate, HttpStatus.OK);
  }

  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/create")
  public ResponseEntity<?> addExchangeRate(@Valid @RequestBody AddExchangeRateDto newExchangeRate, BindingResult result) {
    if (result.hasErrors()) {
      return new ResponseEntity<>(new MessageDto("Request Invalid"), HttpStatus.BAD_REQUEST);
    }
    ExchangeRate exchangeRate = new ExchangeRate(newExchangeRate.getCurrencyFrom(), newExchangeRate.getCurrencyTo(), newExchangeRate.getRate(), new Date());
    exchangeRateService.addExchange(exchangeRate);
    return new ResponseEntity<>(exchangeRate, HttpStatus.OK);
  }

}
