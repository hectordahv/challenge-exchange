package com.challenge.exchange.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.exchange.dto.AddExchangeDto;
import com.challenge.exchange.dto.MessageDto;
import com.challenge.exchange.entities.Exchange;
import com.challenge.exchange.entities.ExchangeRate;
import com.challenge.exchange.services.ExchangeRateService;
import com.challenge.exchange.services.ExchangeService;

@RestController
@RequestMapping("/api/exchange")
@CrossOrigin
public class ExchangeController {

  @Autowired
  ExchangeService exchangeService;

  @Autowired
  ExchangeRateService exchangeRateService;
  
  @PostMapping("/create")
  public ResponseEntity<Exchange> registerExchange(@Valid @RequestBody AddExchangeDto newExchange, BindingResult result) {
    if (result.hasErrors()) {
      return new ResponseEntity(new MessageDto("Request Invalid"), HttpStatus.BAD_REQUEST);
    }
    if (!exchangeRateService.existsByCurrencyFromAndCurrencyTo(newExchange.getCurrencyFrom(), newExchange.getCurrencyTo())) {
      return new ResponseEntity(new MessageDto("Exchange rate not found"), HttpStatus.NOT_FOUND);
    }

    Optional<ExchangeRate> exchangeRate = exchangeRateService.findByCurrencyFromAndCurrencyTo(newExchange.getCurrencyFrom(), newExchange.getCurrencyTo());

    String userLogged = SecurityContextHolder.getContext().getAuthentication().getName();

    Exchange exchange = new Exchange(
      newExchange.getCurrencyFrom(), 
      newExchange.getCurrencyTo(), 
      exchangeRate.get().getRate(), 
      newExchange.getAmount(), 
      newExchange.getAmount() * exchangeRate.get().getRate(), 
      new Date(),
      userLogged
    );
    exchangeService.addExchange(exchange);
    return new ResponseEntity<>(exchange, HttpStatus.OK);
  }
  
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/list")
  public ResponseEntity<List<Exchange>> getAllExchanges(
    @RequestParam("exchangeBy") Optional<String> exchangeBy,
    @RequestParam("currencyFrom") Optional<String> currencyFrom,
    @RequestParam("currencyTo") Optional<String> currencyTo
    ) {
    if (exchangeBy.isPresent()) {
      return new ResponseEntity<>(exchangeService.findByExchangeBy(exchangeBy.get()), HttpStatus.OK);
    }
    if (currencyFrom.isPresent() && currencyTo.isPresent()) {
      return new ResponseEntity<>(exchangeService.findByCurrencyFromAndCurrencyTo(currencyFrom.get(), currencyTo.get()), HttpStatus.OK);
    }
    if (currencyFrom.isPresent()) {
      return new ResponseEntity<>(exchangeService.findByCurrencyFrom(currencyFrom.get()), HttpStatus.OK);
    }
    if (currencyTo.isPresent()) {
      return new ResponseEntity<>(exchangeService.findByCurrencyTo(currencyTo.get()), HttpStatus.OK);
    }
    return new ResponseEntity<>(exchangeService.findAll(), HttpStatus.OK);
  }

}
