package com.challenge.exchange.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class ExchangeRate {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private Long id;

  @NotNull
  private String currencyFrom;

  @NotNull
  private String currencyTo;

  @NotNull
  private Double rate;

  @NotNull
  private Date exchangeDate;

  public ExchangeRate() {
  }

  public ExchangeRate(@NotNull String currencyFrom, @NotNull String currencyTo, @NotNull Double rate, Date exchangeDate) {
    this.currencyFrom = currencyFrom;
    this.currencyTo = currencyTo;
    this.rate = rate;
    this.exchangeDate = exchangeDate;
  }

  public String getCurrencyFrom() {
    return currencyFrom;
  }

  public void setCurrencyFrom(String currencyFrom) {
    this.currencyFrom = currencyFrom;
  }

  public String getCurrencyTo() {
    return currencyTo;
  }

  public void setCurrencyTo(String currencyTo) {
    this.currencyTo = currencyTo;
  }

  public Double getRate() {
    return rate;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }

  public Date getExchangeDate() {
    return exchangeDate;
  }

  public void setExchangeDate(Date exchangeDate) {
    this.exchangeDate = exchangeDate;
  }

}
