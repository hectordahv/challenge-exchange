package com.challenge.exchange.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Exchange {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  private Long id;

  @NotNull
  private String currencyFrom;

  @NotNull
  private String currencyTo;

  @NotNull
  private Double rate;

  @NotNull
  private Double amount;

  @NotNull
  private Double finalAmount;

  @NotNull
  @Column(updatable = false)
  private Date exchangeDate;

  @NotNull
  @Column(updatable = false)
  private String exchangeBy;

  public Exchange() {
  }

  public Exchange(@NotNull String currencyFrom, @NotNull String currencyTo, @NotNull Double rate, @NotNull Double amount, Double finalAmount, Date exchangeDate,
      String exchangeBy) {
    this.currencyFrom = currencyFrom;
    this.currencyTo = currencyTo;
    this.rate = rate;
    this.amount = amount;
    this.finalAmount = finalAmount;
    this.exchangeDate = exchangeDate;
    this.exchangeBy = exchangeBy;
  }

  public String getCurrencyFrom() {
    return currencyFrom;
  }

  public void setCurrencyFrom(String currencyFrom) {
    this.currencyFrom = currencyFrom;
  }

  public String getCurrencyTo() {
    return currencyTo;
  }

  public void setCurrencyTo(String currencyTo) {
    this.currencyTo = currencyTo;
  }

  public Double getRate() {
    return rate;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }

  public Date getExchangeDate() {
    return exchangeDate;
  }

  public void setExchangeDate(Date exchangeDate) {
    this.exchangeDate = exchangeDate;
  }

  public String getExchangeBy() {
    return exchangeBy;
  }

  public void setExchangeBy(String exchangeBy) {
    this.exchangeBy = exchangeBy;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Double getFinalAmount() {
    return finalAmount;
  }

  public void setFinalAmount(Double finalAmount) {
    this.finalAmount = finalAmount;
  }

}
